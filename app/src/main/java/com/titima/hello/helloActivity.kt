package com.titima.hello

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class helloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hello_activity)

        val  displayName: TextView =findViewById(R.id.textView4)
        val name: String = intent.getStringExtra("name").toString()
        displayName.text=name
    }
}