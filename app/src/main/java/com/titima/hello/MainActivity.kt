package com.titima.hello

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    val TAG="Hello:"
    fun hello(view: View?) {
        val name: TextView = findViewById(R.id.textView)
        val id:TextView = findViewById(R.id.textView2)
        Log.d(TAG, "name: ${name.text} | stuid: ${id.text}")
        val i = Intent(this, helloActivity::class.java)
        i.putExtra("name",name.text)
        startActivity(i)
    }
}